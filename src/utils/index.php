<?php
session_start();
include 'conn.php';
include 'content_provider.php';
include 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

require ('./CorsSlim.php');
$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);

$cors = new \CorsSlim\CorsSlim($corsOptions);
$app = new \Slim\Slim();
$app->add($cors);

$current_user = NULL;
$provider = new ContentProvider(getConnection());

#User
$app->post('/login',function()use($app){
  require_once 'user.php';
  $values = getRequestData();
  $username = $values['username'];
  $password = $values['password'];
  $user = new User();
  $login = $user->login($username,$password);

 if ($login >0) {
    $_SESSION['user']=$login;
    $_SESSION['token']=$login['token'];
    echo json_encode($login);
  }else {
    $response['error'] = $login?"Username doesn't exist":"Password incorrect";
    echo json_encode($response);
  }

});

$app->get('/logout',function (){
    session_destroy();
    $response['logout'] =true;
    echo json_encode($response);
});

$app->post('/users',authorizeAdmin(),function(){
  require_once dirname(__FILE__).'/user.php';
  $values = getRequestData();
  $user = new User();
  echo json_encode($user->createUser($values));
});

$app->get('/users',authorize(),function(){
  require_once dirname(__FILE__).'/user.php';
  $users= new User();
  echo json_encode($users->getAll());
});

$app->put('/users/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/user.php';
  $values = getRequestData();
  $user = new User();
  echo json_encode($user->update($id,$values));
});

$app->delete('/users/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/user.php';
  $user= new User();
  echo json_encode($user->delete($id));
});

$app->post('/password/change/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/user.php';
  $values = getRequestData();
  $user = new User();
  echo json_encode($user->changePassword($id,$values));
});

$app->post('/password/reset/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/user.php';
  $values = getRequestData();
  $user = new User();
  echo json_encode($user->resetPassword($id,$values));
});

$app->get('/validate/password/:id',authorize(),function($user_id)use($app){
  require_once dirname(__FILE__).'/user.php';
  $user = new User();
  $old = $app->request->get('old');

 if($user->validate($user_id,$old)){
    //User is logged in and has the correct permissions... Nice!
    $app->halt(200, 'Ok');
  } else {
    // If a user is logged in, but doesn't have permissions, return 403
    $app->halt(400, 'Incorrect password');
  }
  echo json_encode();
});

#Date
$app->get('/dates/:item',authorize(),function($table){
  require_once dirname(__FILE__).'/date.php';
  $date= new Date();
  echo json_encode($date->getFor($table));
});

#Pages
$app->get('/pages/:table',authorize(),function($table){
  require_once dirname(__FILE__).'/content_provider.php';
  $provider = new ContentProvider();
  $count = $provider->count($table);
  $pages = (Integer)($count/20);
  echo $count%20?$pages+1:$pages;
});

#SpecificPages
$app->get('/pages/:table/id/:id',authorize(),function($table,$id){
  require_once dirname(__FILE__).'/content_provider.php';
  $provider = new ContentProvider();
  $selection = "customer_id = $id";
  $count = $provider->specificCount($table,$selection);
  $pages = (Integer)($count/20);
  echo $count%20?$pages+1:$pages;
});


#Utills
$app->get('/utills/last_orders',authorize(),function(){
  require_once dirname(__FILE__).'/utills.php';
    echo json_encode(Utills::getLastOrders());
});

$app->get('/utills/last_stock',authorize(),function(){
  require_once dirname(__FILE__).'/utills.php';
  echo json_encode(Utills::getLastStock());
});

#Statements
$app->post('/statements',authorizeAdmin(),function(){
  require_once dirname(__FILE__).'/statements.php';
  $values = getRequestData();
  $stmt= new Statement();
  echo json_encode($stmt->create($values));
});

$app->get('/statements/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/statements.php';
  $stmt= new Statement();
  echo json_encode($stmt->get($id));
});

$app->get('/statements/for/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/statements.php';
  $stmt= new Statement();
  echo json_encode($stmt->getFor($id));
});

$app->get('/statements/payments/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/statements.php';
  $stmt = new Statement();
  echo json_encode($stmt->getPaidInvoices($id));
});

$app->get('/statements/debt/:id',authorize(),function($customer_id){
  require_once dirname(__FILE__).'/statements.php';
  $stmt = new Statement();
  echo $stmt->getDebt($customer_id);
});

$app->get('/statements/unpaid/:id',authorize(),function($customer_id){
  require_once dirname(__FILE__).'/statements.php';
  $stmt = new Statement();
  $date = null;

  if(isset($_GET['date'])){
    $date = $_GET['date'];
  }

  echo json_encode($stmt->getUnpaidInvoices($customer_id,$date));
});

$app->get('/statements/customers/:id/page/:page',authorize(),function($id,$page){
  require_once dirname(__FILE__).'/statements.php';
  $stmt= new Statement();
  echo json_encode($stmt->getPageFor($id,$page));
});

$app->delete('/statements/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/statements.php';
  $stmt = new Statement();
  echo json_encode($stmt->delete($id));
});


$app->get('/validate/statements/:id',authorize(),function($customer_id)use($app){
  require_once dirname(__FILE__).'/statements.php';

  $stmt = new Statement();
  $amount = $app->request->get('amount');
  $debt = $stmt->getDebt($customer_id);

  if($debt>=$amount){
    //User is logged in and has the correct permissions... Nice!
    $app->halt(200, 'Ok');
  } else {
    // If a user is logged in, but doesn't have permissions, return 403
    $app->halt(400, 'Amount exceeds customer\'s debt');
  }
  echo json_encode();
});

#Batch
$app->post('/batch',authorize(),function(){
  require_once dirname(__FILE__).'/batch.php';
  $values = getRequestData();
  $batch = new Batch();
  echo json_encode($batch->create($values));
});

$app->get('/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/batch.php';
  $batch= new Batch();
  echo json_encode($batch->get($id));
});

$app->get('/batch',authorize(),function(){
  require_once dirname(__FILE__).'/batch.php';
  $batch= new Batch();
  echo json_encode($batch->getAll());
});

$app->get('/batch/for/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/batch.php';
  $batch= new Batch();
  echo json_encode($batch->getFor($id));
});

$app->get('/batch/search/:query',authorize(),function($query){
  require_once dirname(__FILE__).'/orders.php';
  $batch= new Batch();
  echo json_encode($batch->search($query));
});


$app->put('/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/batch.php';
  $values = getRequestData();
  $batch = new Batch();
  echo json_encode($batch->update($id,$values));
});

$app->delete('/batch/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/batch.php';
  $batch = new Batch();
  echo json_encode($batch->delete($id));
});

#Deaths
$app->post('/deaths',authorize(),function(){
  require_once dirname(__FILE__).'/death.php';
  $values = getRequestData();
  $death = new Deaths();
  echo json_encode($death->create($values));
});

$app->get('/deaths/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/death.php';
  $death= new Deaths();
  echo json_encode($death->getFor($id));
});

$app->put('/deaths/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/death.php';
  $values = getRequestData();
  $death = new Deaths();
  echo json_encode($death->update($id,$values));
});

$app->delete('/deaths/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/death.php';
  $death = new Deaths();
  echo json_encode($death->delete($id));
});

#Feeds
$app->post('/feeds',authorize(),function(){
  require_once dirname(__FILE__).'/feeds.php';
  $values = getRequestData();
  $feeds = new Feeds();
  echo json_encode($feeds->create($values));
});

$app->get('/feeds/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/feeds.php';
  $feeds= new Feeds();
  echo json_encode($feeds->getFor($id));
});

$app->put('/feeds/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/feeds.php';
  $values = getRequestData();
  $feeds = new Feeds();
  echo json_encode($feeds->update($id,$values));
});

$app->delete('/feeds/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/feeds.php';
  $feeds = new Feeds();
  echo json_encode($feeds->delete($id));
});

#Consumption
$app->post('/consumption',authorize(),function(){
  require_once dirname(__FILE__).'/consumption.php';
  $values = getRequestData();
  $consumption = new Consumption();
  echo json_encode($consumption->create($values));
});

$app->get('/consumption/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/consumption.php';
  $consumption= new Consumption();
  echo json_encode($consumption->getFor($id));
});

$app->put('/consumption/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/consumption.php';
  $values = getRequestData();
  $consumption = new Consumption();
  echo json_encode($consumption->update($id,$values));
});

$app->delete('/consumption/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/consumption.php';
  $consumption = new Consumption();
  echo json_encode($consumption->delete($id));
});

#Formular
$app->post('/formular',authorize(),function(){
  require_once dirname(__FILE__).'/formular.php';
  $values = getRequestData();
  $formular = new Formular();
  echo json_encode($formular->create($values));
});

$app->get('/formular',authorize(),function(){
  require_once dirname(__FILE__).'/formular.php';
  $formular= new Formular();
  echo json_encode($formular->getAll());
});

$app->get('/formular/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/formular.php';
  $formular= new Formular();
  echo json_encode($formular->getDetails($id));
});


$app->put('/formular/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/formular.php';
  $values = getRequestData();
  $formular = new Formular();
  echo json_encode($formular->update($id,$values));
});

$app->delete('/formular/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/formular.php';
  $formular = new Formular();
  echo json_encode($formular->delete($id));
});


#Ingredients
$app->post('/ingredients',authorize(),function(){
  require_once dirname(__FILE__).'/ingredients.php';
  $values = getRequestData();
  $ingredients = new Ingredients();
  echo json_encode($ingredients->create($values));
});

$app->get('/formulars/ingredients/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/ingredients.php';
  $ingredients= new Ingredients();
  echo json_encode($ingredients->getFor($id));
});

$app->put('/ingredients/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/ingredients.php';
  $values = getRequestData();
  $ingredients = new Ingredients();
  echo json_encode($ingredients->update($id,$values));
});

$app->delete('/ingredients/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/ingredients.php';
  $ingredients = new Ingredients();
  echo json_encode($ingredients->delete($id));
});




#Branches
$app->post('/branches',authorize(),function(){
  require_once dirname(__FILE__).'/branches.php';
  $values = getRequestData();
  $branch = new Branch();
  echo json_encode($branch->create($values));
});

$app->get('/branches/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/branches.php';
  $branches= new Branch();
  echo json_encode($branches->get($id));
});

$app->get('/branches/for/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/branches.php';
  $branches= new Branch();
  echo json_encode($branches->getFor($id));
});

$app->put('/branches/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/branches.php';
  $values = getRequestData();
  $branch = new Branch();
  echo json_encode($branch->update($id,$values));
});

$app->delete('/branches/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/branches.php';
  $branch = new Branch();
  echo json_encode($branch->delete($id));
});

#Sales
$app->post('/sales',authorize(),function(){
  require_once dirname(__FILE__).'/sales.php';
  $values = getRequestData();
  $sale = new Sales();
  echo json_encode($sale->create($values));
});

$app->get('/sales/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/sales.php';
  $sales= new Sales();
  echo json_encode($sales->getFor($id));
});

$app->delete('/sales/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/sales.php';
  $sale = new Sales();
  echo json_encode($branch->delete($id));
});

#Gifts
$app->post('/gifts',authorize(),function(){
  require_once dirname(__FILE__).'/gifts.php';
  $values = getRequestData();
  $gift= new Gifts();
  echo json_encode($gift->create($values));
});

$app->get('/gifts',authorize(),function(){
  require_once dirname(__FILE__).'/gifts.php';
  $gifts= new Gifts();
  echo json_encode($gifts->getAll());
});

$app->get('/gifts/date/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/gifts.php';
  $gifts= new Gifts();
  echo json_encode($gifts->getForDate($date));
});

$app->get('/gifts/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/gifts.php';
  $gifts= new Gifts();
  echo json_encode($gifts->getPage($page));
});

$app->delete('/gifts/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/gifts.php';
  $gift = new Gifts();
  echo json_encode($gift->delete($id));
});

#Expences
$app->post('/expences',authorize(),function(){
  require_once dirname(__FILE__).'/expences.php';
  $values = getRequestData();
  $expence= new Expences();
  echo json_encode($expence->create($values));
});

$app->get('/expences/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/expences.php';
  $expences= new Expences();
  echo json_encode($expences->getForBatch($id));
});

$app->get('/expences/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/expences.php';
  $expences= new Expences();
  echo json_encode($expences->getDetails($id));
});

$app->get('/expences/items/:batch/:date',authorize(),function($batch_id,$date){
  require_once dirname(__FILE__).'/expences.php';
  $expences= new Expences();
  echo json_encode($expences->getItemsFor($date,$batch_id));
});

$app->get('/expences/date/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/expences.php';
  $expences= new Expences();
  echo json_encode($expences->getForDate($date));
});

$app->get('/expences/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/expences.php';
  $expences= new Expences();
  echo json_encode($expences->getPage($page));
});


$app->delete('/expences/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/expences.php';
  $expence = new Expences();
  echo json_encode($expence->delete($id));
});

$app->delete('/expences/items/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/expenceItem.php';
  $expence = new Expences();
  echo json_encode($expence->delete($id));
});

$app->post('/expences/category',authorize(),function(){
  require_once dirname(__FILE__).'/expences.php';
  $values = getRequestData();
  $expence= new Expences();
  echo json_encode($expence->createCategory($values));
});

$app->get('/expences/category/all',function(){
  require_once dirname(__FILE__).'/expences.php';
  $expences= new Expences();
  echo json_encode($expences->getCategories());
});

$app->delete('/expences/category/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/expences.php';
  $expence = new Expences();
  echo json_encode($expence->deleteCategory($id));
});



#Orders
$app->post('/orders',authorize(),function(){
  require_once dirname(__FILE__).'/orders.php';
  $values = getRequestData();
  $order = new Order();
  echo json_encode($order->create($values));
});

$app->get('/orders/batch/:id',authorize(),function($batch_id){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->getForBatch($batch_id));
});

$app->get('/orders/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->getPage($page));
});

$app->get('/orders/date/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->getForDate($date));
});

$app->get('/orders/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->get($id));
});

$app->get('/orders/customers/:id/date/:date',authorize(),function($id,$date){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->getFor($id,$date));
});

$app->get('/orders/customers/:id/page/:page',authorize(),function($id,$page){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->getPageFor($id,$page));
});

$app->get('/orders/details/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->getOrderDetails($id));
});

$app->get('/orders/search/:query',authorize(),function($query){
  require_once dirname(__FILE__).'/orders.php';
  $orders= new Order();
  echo json_encode($orders->search($query));
});

$app->put('/orders/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/orders.php';
  $values = getRequestData();
  $order = new Order();
  echo json_encode($order->update($id,$values));
});

$app->delete('/orders/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/orders.php';
  $order = new Order();
  echo json_encode($order->delete($id));
});


#Products
$app->post('/products',authorize(),function(){
  require_once dirname(__FILE__).'/products.php';
  $values = getRequestData();
  $product = new Product();
  echo json_encode($product->create($values));
});

$app->get('/products',authorize(),function(){
  require_once dirname(__FILE__).'/products.php';
  $products= new Product();
  echo json_encode($products->getAll());
});

$app->put('/products/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/products.php';
  $values = getRequestData();
  $product = new Product();
  echo json_encode($product->update($id,$values));
});

$app->delete('/products/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/products.php';
  $product = new Product();
  echo json_encode($product->delete($id));
});

#Costing
$app->post('/costing',authorize(),function(){
  require_once dirname(__FILE__).'/costing.php';
  $values = getRequestData();
  $costing = new Costing();
  echo json_encode($costing->create($values));
});

$app->get('/costing',authorize(),function(){
  require_once dirname(__FILE__).'/costing.php';
  $costing= new Costing();
  echo json_encode($costing->getAll());
});

$app->get('/costing/date/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/costing.php';
  $costing= new Costing();
  echo json_encode($costing->getForDate($date));
});

$app->get('/costing/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/costing.php';
  $costing= new Costing();
  echo json_encode($costing->getPage($page));
});

$app->get('/costing/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/costing.php';
  $costing= new Costing();
  echo json_encode($costing->get($id));
});

$app->put('/costing/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/costing.php';
  $values = getRequestData();
  $costing = new Costing();
  echo json_encode($costing->update($id,$values));
});

$app->delete('/costing/:id',authorize(),function($id){
  require_once dirname(__FILE__) . '/costing.php';
  $costing = new Costing();
  echo json_encode($costing->delete($id));
});

#Cash
$app->post('/cash',authorize(),function(){
  require_once dirname(__FILE__).'/cash.php';
  $values = getRequestData();
  $cash = new Cash();
  echo json_encode($cash->create($values));
});

$app->get('/cash/batch/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/cash.php';
  $cash= new Cash();
  echo json_encode($cash->getCash($id));
});

$app->get('/cash/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/cash.php';
  $cash= new Cash();
  echo json_encode($cash->getPage($page));
});

$app->get('/cash/group/:id/:date',authorize(),function($batch_id,$date){
  require_once dirname(__FILE__).'/cash.php';
  $cash= new Cash();
  echo json_encode($cash->getForGroup($batch_id,$date));
});

$app->get('/cash/date/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/cash.php';
  $cash= new Cash();
  echo json_encode($cash->getForDate($date));
});

$app->get('/cash/search/:query',authorize(),function($query){
  require_once dirname(__FILE__).'/cash.php';
  $cash= new Cash();
  echo json_encode($cash->search($query));
});

$app->delete('/cash/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/cash.php';
  $cash = new Cash();
  echo json_encode($cash->delete($id));
});

#Stock
$app->post('/stock',authorize(),function(){
  require_once dirname(__FILE__).'/stock.php';
  $values = getRequestData();
  $stock = new Stock();
  echo json_encode($stock->create($values));
});

$app->get('/stock',authorize(),function(){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->getAll());
});

$app->get('/stock/date/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->getForDate($date));
});

$app->get('/stock/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->getPage($page));
});

$app->get('/stock/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->getFor($id));
});

$app->put('/stock/:id',authorize(),function($id){
  require_once dirname(__FILE__).'/stock.php';
  $values = getRequestData();
  $stock = new Stock();
  echo json_encode($stock->update($id,$values));
});

$app->delete('/stock/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->delete($id));
});

$app->get('/summary/stock',authorize(),function(){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->getSummary());
});

$app->get('/summary/stock/:date',authorize(),function($date){
  require_once dirname(__FILE__).'/stock.php';
  $stock= new Stock();
  echo json_encode($stock->getSummaryDate($date));
});

$app->get('/stock_summary/page/:page',authorize(),function($page){
  require_once dirname(__FILE__).'/stock.php';
  $summary= new Stock();
  echo json_encode($summary->getSummaryPage($page));
});


$app->get('/validate/stock/:product_code',authorize(),function($code)use($app){
  require_once dirname(__FILE__).'/stock.php';
    $stock= new Stock();
    $q = $app->request->get('quantity');
    $date = date("Y-m");

    if ($stock->getAvailabel($code,$date)>=$q) {
      //User is logged in and has the correct permissions... Nice!
      $app->halt(200, 'Ok');
    } else {
      // If a user is logged in, but doesn't have permissions, return 403
      $app->halt(400, 'Quantity not available in stock');
    }

});

#Customers
$app->post('/customers',authorizeAdmin(),function(){
  require_once dirname(__FILE__) . '/customer.php';
  $values = getRequestData();
  $customer = new Customer();
  echo json_encode($customer->create($values));
});

$app->get('/customers',authorize(),function(){
  require_once dirname(__FILE__) . '/customer.php';
  $customer = new Customer();
  echo json_encode($customer->getAll());

});

$app->get('/temp/customers',authorize(),function(){
  require_once dirname(__FILE__) . '/customer.php';
  $customer = new Customer();
  echo json_encode($customer->getTemp());

});

$app->get('/customers/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/customer.php';
  $customer = new Customer();
  echo json_encode($customer->get($id));

});

$app->delete('/customers/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/customer.php';
  $customer = new Customer();
  echo json_encode($customer->delete($id));
});

$app->put('/customers/:id',authorizeAdmin(),function($id){
  require_once dirname(__FILE__) . '/customer.php';
  $values = getRequestData();
  $customer = new Customer();
  echo json_encode($customer->update($id,$values));
});

$app->get('/customers/search/:query',authorizeAdmin(),function($query){
  require_once dirname(__FILE__) . '/customer.php';
  $customer = new Customer();
  echo json_encode($customer->search($query));
});


$app->get('/temp/customers/search/:query',authorize(),function($query){
  require_once dirname(__FILE__) . '/customer.php';
  $customer = new Customer();
  echo json_encode($customer->searchTemp($query));
});

$app->get('/stats/month/:month',function($month){
  require_once dirname(__FILE__) . '/stats.php';
  $stats = new Stats();
  echo json_encode($stats->getMonthStats($month));
});

$app->get('/stats/year/:year',function($year){
  require_once dirname(__FILE__) . '/stats.php';
  $stats = new Stats();
  echo json_encode($stats->getYearStats($year));
});

$app->get('/stats/range/:from/:to',function($from,$to){
  require_once dirname(__FILE__) . '/stats.php';
  $stats = new Stats();
  echo json_encode($stats->getRangeStats($from,$to));
});


$app->run();

function getRequestData(){
  $data = (array)json_decode(file_get_contents('php://input'));
  return $data;
}

/**
 * Authorise function, used as Slim Route Middlewear (http://www.slimframework.com/documentation/stable#routing-middleware)
 */
function authorize() {

    return function(){
//        echo 'Role.....'.$role;
// Get the Slim framework object
        $app  = \Slim\Slim::getInstance();

        if (!empty($_SESSION['token'])) {
          // Next, validate the role to make sure they can access the route
          // We will assume admin role can access everything
          return true;
        } else {
          // If a user is not logged in at all, return a 401
          $app->halt(401, 'Unauthorized access');
        }
      };
// First, check to see if the user is logged in at all
}


function authorizeAdmin() {

    return function(){
//        echo 'Role.....'.$role;
// Get the Slim framework object
        $app  = \Slim\Slim::getInstance();
        if (!empty($_SESSION['token'])) {
          // Next, validate the role to make sure they can access the route
          // We will assume admin role can access everything
          if ($_SESSION['user']['role']=="Admin") {
            //User is logged in and has the correct permissions... Nice!
            return true;
          } else {
            // If a user is logged in, but doesn't have permissions, return 403
            $app->halt(403, 'Unauthorized access');
          }
        } else {
          // If a user is not logged in at all, return a 401
          $app->halt(401, 'Unauthorized access');
        }
      };
// First, check to see if the user is logged in at all
}
