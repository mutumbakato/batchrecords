<?php
/**
*This class handles all default operations
*/
class Model{

  protected $provider = null;
  protected $table = null;

  function __construct() {

  }

  public function create($values){

    $values['user'] = $this->getUserId($_SESSION['user']['token']);
    $result = $this->provider->insert($this->table,$values);

    if ($result) {
      return $this->getDetails($result);
    }else{
      echo $this->provider->message;
      return ['error'=>true,'message'=>$this->provider->message];
    }
    return 0;
  }

  public function getAll(){
    return $this->provider->query($this->table,'*');
  }

  public function delete($id){
    return $this->provider->delete($this->table,'id=?',[0=>$id]);
  }

  public function update($id,$values){
    $results = $this->provider->update($this->table,'id=?',[0=>$id],$values);
    if($results){
      return $results;
    }else {
      echo $this->provider->message;
      return 0;
    }
  }

  public function getDetails($id){
    return  $this->provider->query($this->table,'*','id=?',[0=>$id]);
  }

  public function getUserId($token) {
    $results = $this->provider->query('users','id','token=?',[0=>$token]);
    if ($results) {
      return $results['id'];
    } else {
      return NULL;
    }
  }
}
