<?php
/**
* Description of ContentProvider
*This is a helper class for database CRUD operatios and syncronization
* @author katoj
*/
class DbHelper {

  public $message = "";
  private $_dbh;
  private $syncResults;

  function __construct() {
    require_once dirname(__FILE__) . '/conn.php';
    $this->_dbh = getConnection();
  }
  /**
  * Insert  values
  *@param String $table the table name
  *@param Array $values assoc array with table fields as keys and insert values as values
  *
  */
  public function insert($table, $values) {

    $values = (array) $values;

    //Retriving the field names from array
    //NOTE: The key represents a field name
    foreach ($values as $key => $value) {
      $fields[]= $key;
    }

    //Generate field projection
    $projection = '('.implode(',',$fields).')';
    //Generate question marks
    $value_holder = '('.$this->placeholders('?',sizeof($values)).')';
    //Generate insert values
    $content_values = array_values($values);
    //Prepareing the PDO statement
    $query = "INSERT INTO  $table  $projection VALUES  $value_holder ";
    $stmt = $this->_dbh->prepare($query);

    try {
      $stmt->execute($content_values);
      return $this->_dbh->lastInsertId();
    } catch (Exception $ex) {
      $this->message.= $ex->getMessage().$query;
      return 0;
    }
  }

  /**
  *@deprecated Because it has too mayn params that are not easy to remember
  *
  */
  public function query($table, $projection, $selection=null, $selectionArgs=null, $group=null,$sortOrder = null, $limit = null, $isArray = false) {
    $query = "SELECT $projection  FROM  $table ";
    if (!is_null($selection)) {
      $query.=" WHERE " . $selection;
    }
    if (!is_null($group)) {
      $query.=" GROUP BY " .$group;
    }
    if (!is_null($sortOrder)) {
      $query.=" ORDER BY " . $sortOrder;
    }
    if (!is_null($limit)) {
      $query.=" LIMIT " . $limit;
    }
    $this->message.=$query;

    try {
      $stmt = $this->_dbh->prepare($query);
      if(!is_null($selectionArgs)){
        $stmt->execute($selectionArgs);
      }else{
        $stmt->execute();
      }

      if ($stmt->rowCount() == 1 && !$isArray) {
        return (array) $stmt->fetchObject();
      } elseif ($stmt->rowCount() > 1 || $isArray) {
        return (array) $stmt->fetchAll(PDO::FETCH_OBJ);
      } else {
        return -1;
      }

    } catch (Exception $exc) {
      $this->message.=$exc->getMessage();
      return 0;
    }
  }

  public function query_($table, $options = []) {
    $projection = $options['projection']?$options['projection']:'*';
    $query = "SELECT $projection FROM  $table";
    $isArray = !empty($options['is_array']);

    if (!empty($options['selection'])) {
      $query.=" WHERE " . $options['selection'];
    }
    if (!empty($options['group'])) {
      $query.=" GROUP BY " .$options['group'];
    }
    if (!empty($options['sort'])) {
      $query.=" ORDER BY " . $options['sort'];
    }
    if (!empty($options['limit'])) {
      $query.=" LIMIT " . $options['limit'];
    }

    $this->message.=$query;
    try {
      $stmt = $this->_dbh->prepare($query);
      if(!is_null($options['args'])){
        $stmt->execute($options['args']);
      }else{
        $stmt->execute();
      }
      if ($stmt->rowCount() == 1 && !$isArray) {
        return (array) $stmt->fetchObject();
      } elseif ($stmt->rowCount() > 1 || $isArray) {
        return (array) $stmt->fetchAll(PDO::FETCH_OBJ);
      } else {
        return -1;
      }
    } catch (Exception $exc) {
      $this->message.=$exc->getMessage();
      return 0;
    }
  }

  public function update($table, $selection, $selectionArgs, $values) {
    $values = (array) $values;

    //Retriving the field names from array
    //NOTE: The key represents a field name
    foreach ($values as $key => $value) {
      $fields[]= $key.'=?';
    }

    //Generate field projection
    $projection = implode(',',$fields);

    //Generate insert values
    $content_values = array_values($values);

    //Add selectionArgs to  content_values
    foreach ($selectionArgs as  $arg) {
      $content_values[] = $arg;
    }

    $query = "UPDATE $table  SET  $projection  WHERE  $selection";
    //$this->message.=$query;
    try {
      $stmt = $this->_dbh->prepare($query);
      $stmt->execute($content_values);
      return $stmt->rowCount();
    } catch (Exception $exc) {
      $this->message.=$exc->getMessage();
      return 0;
    }
  }

//You can change the delete logic to just change the state to deleted as is not a good idea to do the actual delete
  public function delete($table, $selection, $selectionArgs) {
    try {
      $stmt = $this->_dbh->prepare("DELETE FROM  $table  WHERE $selection");
      $stmt->execute($selectionArgs);
      return $stmt->rowCount();
    } catch (Exception $exc) {
      $this->message.= $exc->getMessage();
      return 0;
    }
  }

  //Takes the datble name and an array or data objects
  public function insertMultiple($table, $data) {
    //Retriving the field names from the first data set
    //NOTE: The key should be the same as a field name
    $values = (array) $data[0];
    foreach ($values as $key => $value) {
      $fields[] = $key;
    }
    //Generate projection
    $projection =' ('.implode(",",$fields).') ';
    //Generate question marks
    $insert_values = array();

    foreach($data as $d){
      $d =(array)$d;
      $question_marks[] = '('.$this->placeholders('?', count($d)).')';
      $insert_values = array_merge($insert_values,array_values($d));
    }

    $sql = "INSERT INTO ".$table.$projection."VALUES " . implode(',', $question_marks);
    $stmt = $this->_dbh->prepare($sql);
    $this->message.=$sql;

    try {
      $stmt->execute($insert_values);
      return true;
    } catch (PDOException $e){
      $this->message.= $e->getMessage();
      return false;
    }
  }

  public function count($table,$selection=null,$args=null){

    if($selection && $args) {
      $query = "select * from $table where $selection";
    }
    else {
      $query = "select * from $table";
    }
    $this->message .= $query;
    $stmt = $this->_dbh->prepare($query);
    try {
      if ($selection && $args) {
        $stmt->execute($args);
      }else {
        $stmt->execute();
      }
      return $stmt->rowCount();

    } catch (Exception $e) {
      $this->message .= $e->getMessage();
    }
    return 0;
  }


  private function  placeholders($text, $count=0, $separator=","){
    $result = array();
    if($count > 0){
      for($x=0; $x<$count; $x++){
        $result[] = $text;
      }
    }
    return implode($separator, $result);
  }

}
