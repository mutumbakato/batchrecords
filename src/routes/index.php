<?php
// Routes

//Index page
$app->get('/', function ($request, $response, $args) {
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

//Formats the response object
function talkBack($response,$data,$error=false,$message="OK",$status=200){

    $res  = array('data' =>$data);
    $res['error'] = $error;

    if ($error) {
     $res['message'] = $message;
    }

    $response = $response->withStatus($status);
    $response->getBody()->write(json_encode($res,JSON_PRETTY_PRINT));

    return $response;
}
