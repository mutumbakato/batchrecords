<?php

//User Routes
//fetch vaccination_schedule with the @param:id
$app->get('/vaccination/schedules', function ($request, $response, $args) {
  $vaccination = new Vaccination($this->logger);
  $results = $vaccination->getPublichSchedules($limit);
  // return talkBack($response,$results ,(!$results||$results == NOT_FOUND),($results== NOT_FOUND? "No Vaccination Schedule found.":"Error retriving Schedules"));
  $response->getBody()->write(json_encode($results,JSON_PRETTY_PRINT));
  return $response;
});

$app->get('/vaccination/schedule_items/[{schedule_id}]', function ($request, $response, $args) {
  $schedule_id = $request->getAttribute('schedule_id');
  $this->logger->info($schedule_id);
  $vaccination = new Vaccination($this->logger);
  $results = $vaccination->getScheduleItems($schedule_id);
  $response->getBody()->write(json_encode($results,JSON_PRETTY_PRINT));
  return $response;
  //return talkBack($response,$results ,(!$results||$results == NOT_FOUND),($results== NOT_FOUND? "No Vaccinations found":"Error retriving Vaccinations"));
});

?>
