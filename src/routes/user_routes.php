<?php

//User Routes
//fetch user with the @param:id
$app->get('/user/[{id}]', function ($request, $response, $args) {
  $id = $request->getAttribute('id');
  $user = new User($this->logger);
  $results = $user->getDetails($id);
  return talkBack($response,$results ,(!$results||$results == NOT_FOUND),($results== NOT_FOUND? "No user found with id: $id":"Error retriving user"));
});

$app->get('/user/phone/[{number}]', function ($request, $response, $args) {
  $phone = $request->getAttribute('number');
  $user = new User($this->logger);
  $results = $user->findByPhone($phone);
  return talkBack($response,$results ,(!$results||$results == NOT_FOUND),($results== NOT_FOUND? "No user found with Phone: $phone":"Error retriving user"));
});

$app->get('/user/get/email', function ($request, $response, $args) {
  $phone = $request->getParam('email');
  $user = new User($this->logger);
  $results = $user->findByEmail($phone);
  return talkBack($response,$results ,(!$results||$results == NOT_FOUND),($results== NOT_FOUND? "No user found with Email: $phone":"Error retriving user"));
});

//User login
$app->get('/user/all/[{page}]', function ($request, $response, $args) {
  $page =  $request->getAttribute('page')?$request->getAttribute('page'):1;
  $user = new User($this->logger);
  $results = $user->getAll($page);
  return talkBack($response,$results ,(!$results||$results == NOT_FOUND),($results== NOT_FOUND? "No users found":"Error retriving users"));
});

//User registration
$app->post('/user/register', function ($request, $response, $args) {

  //Retrieve post data from the request
  $data = $request->getParams();

  //check if the post data is not null
  if (empty($data)) {
    return talkBack($response,[],true,"No data provided");
  }

  $data= (Array)$data;
  $email = $data['email'];
  $username = $data['username'];
  $phone = $data['phone'];

  //log the recieved data
  $user = new User($this->logger);

  //Check if any of username, email or phone exists in the database
  // $is_username = $user->isUserNameExist($username);
  $is_email = $user->isEmailExist($email);
  // $is_phone =$user->isPhoneExist($phone);

  //If any of username, email or phone exists send back an error message
  if ($is_username || $is_email) {
    $message = ($is_username? "Username already exists." : ($is_email? "Email already exists.":"Phone number already exists."));
    return talkBack($response,0,true,$message);
  }

  $results= $user->createAccount($data);
  return talkBack($response,$results,!$results,"An error occured while creating User.");

});

// verify user code and phone number
$app->post('/user/verify', function ($request, $response, $args) {

  $data = $request->getParams();

  if (empty($data['phone']||empty($data['code']))) {
    return talkBack($response,0,true,"Invalid data provided");
  }

  $code = $data['code'];
  $phone = $data['phone'];
  $user = new User($this->logger);
  $results = $user->verify($phone,$code);

  $this->logger->info("FLAG: Verify".$results);

  return talkBack($response,["verified"=>(($results==0||$results==NOT_FOUND)?false:$results)],(!$results||$results == NOT_FOUND),
  (!$results?"Unknown error":"Unknown Distributor code or Phone number"));

});

//user login
$app->post('/user/login', function ($request, $response, $args) {

  // $this->logger->info("User-post ".json_encode($request->getParams()));
  $data = $request->getParams();
  $data = (Array)$data;

  if (!$data['username']||!$data['password']) {
    return talkBack($response,0,true,"Invalid data provided");
  }

  $user = new User($this->logger);
  $results= $user->login($data['username'],$data['password']);
  return talkBack($response,$results,(!$results||$results== NOT_FOUND),(!$results?"Login error!":"Username or Password is incorrect"));
});

//user logout
$app->post('/user/logout', function ($request, $response, $args) {
  $data = $request->getParams();
  $data = (Array)$data;
  if (empty($data['token'])) {
    return talkBack($response,0,true,"Invalid data provided!");
  }
  $user = new User($this->logger);
  $results= $user->logOut($data['token']);
  return talkBack($response,$results,(!$results||$results== NOT_FOUND), (!$results?"LogOut error!":"Inavlid logout details"));
});

//user logout
$app->post('/user/password/forgot', function ($request, $response, $args) {

  $data = $request->getParams();
  $data = (Array)$data;

  if (empty($data['email'])) {
    return talkBack($response,0,true,"Invalid data provided!");
  }

  $email = $data['email'];
  $user = new User($this->logger);
  $results= $user->resetPassword($data['email']);
  return talkBack($response,$results,(!$results||$results== NOT_FOUND), (!$results?"Reset error!":"Email address $email does not exist in the system."));
});

//Index page
$app->get('/user/password/reset/[{token}]', function ($request, $response, $args) {
  // Render index view
  return $this->renderer->render($response, 'password_reset.phtml', $args);
});

//user logout
$app->post('/user/password/update', function ($request, $response, $args) {

  $data = $request->getParams();
  $data = (Array)$data;

  if (empty($data['password'])||empty($data['token'])) {
    return talkBack($response,0,true,"Invalid data provided!");
  }

  $user = new User($this->logger);
  $results= $user->updatePassword($data['password'],$data['token']);
    if(!$results||$results== NOT_FOUND){

      $response->getBody()->write(!$results?"Failed to reset password!":"This link has expired.");

    }else{
      $response->getBody()->write($results);
    }

  return $response;
  //  talkBack($response,$results,(!$results||$results== NOT_FOUND), (!$results?"Failed to reset password!":"Inavalid token."));
});



?>
