<?php

//licence routes
$app->post('/licence/new_key', function ($request, $response, $args) {

  $data = $request->getParams();
  $data = (Array)$data;

  if (!$data['amount']) {
    return talkBack($response,0,true,"Invalid data provided");
  }

  $licence = new Licence($this->logger);
  $amount = $data['amount'];
  $results = $licence->generatekey($amount);

  return talkBack($response,$results,(!$results||$results==NOT_FOUND),(!$results?"Unknown error":"Failed to create Licence Key"));
});

#Activate account through licence key
$app->post('/licence/activate', function ($request, $response, $args) {

  $user_id = $request->getAttribute('user');
  if (!$user_id || $user_id==NOT_FOUND) {
    return talkBack($response,0,true,"Unauthorizes",401);
  }
  $data = $request->getParams();
  $data = (Array)$data;

  if (!$data['key']) {
    return talkBack($response,0,true,"Invalid data provided");
  }

  $key = $data['key'];
  $licence = new Licence($this->logger);

  //Check if user has a licence
  if($licence->hasLicence($user_id)){
    return talkBack($response,"0",true,"You already have a valid Licence");
  }

  //Check if key exists?
  if(!$licence->iskeyExists($key)){
    return talkBack($response,"0",true,"Licence Key does not exist.");
  }

  //Check if key is not used yet.
  if($licence->iskeyUsed($key)){
    return talkBack($response,"0",true,"Licence Key is already used.");
    }

  //Activate key
  $results = $licence->activateLicence($user_id,$key);

  return talkBack($response,['licence'=>$results],(!$results),("Invalid Licence Key"));
});

$app->post('/licence/status', function ($request, $response, $args) {

    $data = $request->getParams();
    $data = (Array)$data;

    if (!$data['user']) {
      return talkBack($response,0,true,"Invalid data provided");
    }

    $user =$data['user'];

    $licence = new Licence($this->logger);
    $results = $licence->getLicence($user);

    return talkBack($response,$results,(!$results||$results==NOT_FOUND),(!$results?"Unknown error":"Licence key unknown"));
  });



?>
