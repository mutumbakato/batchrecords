<?php

//User Routes
//fetch user with the @param:id
$app->post('/sync', function ($request, $response, $args) {

  $user = $request->getAttribute('user');

  $this->logger->info("User_ID === ".$user);

  $sync = new Sync($user,$this->logger);

  $json = file_get_contents('php://input');

  $syncData = json_decode($json);

  $results = $sync->synchronize($syncData);

  return talkBack($response,$results ,(!$results || $results == NOT_FOUND),($results == NOT_FOUND? "No user found with id: $user":"Error retriving user"));
});