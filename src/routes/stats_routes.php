<?php
// Routes

//Index page
$app->get('/stats', function ($request, $response, $args) {
    $stats = new Statistics($this->logger);
    // Render index view
    $results = $stats->getStats();
    return talkBack($response,$results,false);
});

