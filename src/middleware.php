<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(function ($request, $response, $next) {

	$token = "";

	//get token from the request header
  $headers = $request->getheader("Authorization");

  if ($headers) {
   $token = $headers[0];
  }

	$this->logger->info($token);

	//Get user id using a login_token
	$user_id = User::auth($this->logger,$token);

	$this->logger->info("UserID = ".$user_id);

	//Add user_id to the request attributes
	$request = $request->withAttribute('user', $user_id);

	//Continue with request
	$response = $next($request, $response);
	return $response;
});

