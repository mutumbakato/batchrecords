<?php
/**
* Subscription model
*/

class Licence {

  private $db;
  private $logger;

  public function __construct($logger) {
    $this->logger = $logger;
    $this->db = new DbHelper();
  }


  public function generatekey($amount){

    $key = $this->unique_id();

    $date = date('Y-m-d H:i:s');
    $id = time()*1000;

    $status = 'normal';
    $values = ['id'=>$id,'licence_key'=>$key,'date'=>$date,'status'=>$status,'user'=>'0','amount'=>$amount];

    $results = $this->db->insert(TB_LICENCE,$values);

    // if(!$results){
    //   $this->logger->error($this->db->message);
    //   return $results;
    // }

    return $key;
  }

 private function unique_id($l = 8) {
    return substr(md5(uniqid(mt_rand(), true)), 0, $l);
}

  // Returns licence details or -1 if licence is not paid
  public function getLicence($user_id){

    $options['projection'] = "date,status";
    $options['selection'] = "user=?";
    $options['args']= [0=>$user_id];

    $results = $this->db->query_(TB_LICENCE,$options);

    if (!$results || $results == NOT_FOUND) {
      $this->logger->error(json_encode($results));
      $this->logger->error($this->db->message);
      return "none";
    }else{
      return "active";
    }

  }

  public function activateLicence($user_id,$key){

    $values= ["user"=>$user_id];
    $selection = "licence_key = ?";

    $args = [0=>$key];
    $results= $this->db->update(TB_LICENCE,$selection,$args,$values);

    if(!$results){
      $this->logger->error($this->db->message);
      return $results;
    }

    return "active";
  }

  public function iskeyExists($key){
    $results = $this->db->query(TB_LICENCE,"id","licence_key=?",[0=>$key]);
    if(!$results){
      $this->logger->info($this->db->message);
    }
    return ($results && $results!=-1);
  }

  public function iskeyUsed($key){

    $results = $this->db->query(TB_LICENCE,"user","licence_key=?",[0=>$key]);
    if(!$results){
      $this->logger->info($this->db->message);
    }

    return ($results && $results['user']!="0");
  }

  public function hasLicence($user_id){
    return ($this->getLicence($user_id)=="active");
  }

  public function fetch($selection=null,$args=null,$limit){
    $table ="subscriptions s LEFT JOIN plans p ON p.id = s.plan LEFT JOIN payments pp ON pp.subscription_id=s.id LEFT JOIN users u ON u.id = s.user_id ";
    $projection = "s.id,s.transaction_id,s.status,s.status_message,DATE_FORMAT(s.created_date,'%d %M %Y') AS date,s.phone,s.quantity,p.name,IFNULL(pp.amount,0) AS amount,IFNULL(u.username,'Unknown') AS username";
    $results = $this->db->query($table,$projection,$selection,$args,null,null,$limit,true);
    $this->logger->error($this->db->message);

    if (!$results) {
      $this->logger->error($this->db->message);
    }

    return $results;
  }


}
?>
