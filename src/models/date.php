<?php

class Date{
  private $provider = null;

  function __construct(){
    require_once dirname(__FILE__) . '/content_provider.php';
    $this->provider = new ContentProvider();
  }

  public function getFor($table) {
    return $this->provider->query($table,"DATE_FORMAT(date,'%M - %Y') AS name",null,null,"name","DATE_FORMAT(date,'%Y - %m') DESC");
  }

  public function getYearsFor($table) {
    return $this->provider->query($table,"DATE_FORMAT(date,'%Y') AS name",null,null,"name","DATE_FORMAT(date,'%Y - %m') DESC",null,true);
  }

}



 ?>
