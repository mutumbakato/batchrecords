<?php

class Statistics{

  private $stats = array();
  private $db;
  private $logger;

  public function __construct($logger) {
    $this->db = new DbHelper();
    $this->logger = $logger;
    $this->stats = array();
  }

  public function getStats(Type $var = null){
    $this->stats['users'] = $this->getUsers();
    $this->stats['usercount'] = $this->getUserCount();
    $this->stats['activations'] = $this->getActivations();
    $this->stats['revenue'] = $this->getTotalRevenue();
    return $this->stats;
  }

  public function getUsers($limit=null){

    $projection = "id,username,email,phone";
    $results = $this->db->query(TB_USERS,$projection);
    $licence = new Licence($this->logger);

    foreach ($results as $key => $user){
       $results[$key]->licence =  $licence->getLicence($user->id);
    }

    return $results;
  }

  public function getUserCount(){
    return $this->db->count(TB_USERS);
  }

  public function getActivations(){
    return $this->db->count(TB_LICENCE,"user!=?",[0=>"0"]);
  }

  public function getTotalRevenue(){
      $table = TB_LICENCE;
      $projection = "sum(amount) AS revenue";
      $selection = "user !=?";
      $args =[0=>"0"];
      $results = $this->db->query($table,$projection,$selection,$args);
      return $results['revenue'];
  }

  public function get(){
      # code...
  }

}