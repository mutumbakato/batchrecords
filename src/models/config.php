<?php
//Application Constants
define('TB_USERS', 'users');
define('TB_LOGINS', 'logins');
define('TB_ADMIN_LOGINS', 'admin_logins');
define('TB_DEVICES', 'devices');
define('TB_SUBSCRIPTION', 'subscriptions');
define('TB_PLANS', 'plans');
define('TB_PAYMENTS', 'payments');
define('TB_DOWNLOADS', 'downloads');
define('TB_LICENCE', 'licence');
define('TB_VACCINATION','vaccination');
define('TB_VACCINATION_SCHEDULE','vaccination_schedule');
define('TB_FIREBASE', 'firebase');
define('COMPLETE', 'complete');
define('FAILED', 'Failed');
define('PENDING', 'pending');
define('LICENCE', 'licence');
define('SLAVE', 'slave');
define('MASTER', 'master');
define('UNAUTHORIZED', 401);
define('LIMIT', 30);
define('NOT_FOUND', -1);
define('EXISTS', -2);
define('FIREBASE_SEND_URL', 'https://fcm.googleapis.com/fcm/send');
?>
