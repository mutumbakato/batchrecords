<?php

class Vaccination {

    private $db;
    private $logger;

    public function __construct($logger) {
      $this->db = new DbHelper();
      $this->logger = $logger;
    }

public function getPublichSchedules($limit){
    $results =$this->db->query(TB_VACCINATION_SCHEDULE,"*","publicity=?",[0=>"public"],NULL,NULL,NULL,true);
    if(!$results  || $results==NOT_FOUND){
        $this->logger->error($this->db->message);
     }
    return $results;
}

public function getScheduleItems($schedule_id){
    $results =$this->db->query(TB_VACCINATION,"*","scheduleId=?",[0=>$schedule_id],NULL,NULL,NULL,true);
    if(!$results || $results==-1){
        $this->logger->error($this->db->message);
    }
    return $results;
}

}