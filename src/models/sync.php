<?php
/**
 * Synchronization model, handles data sychornization between client apps and remote database
 */
class Sync{

  private $syncResults = array();
  private $db;
  private $logger;
  private $user;

  public function __construct($user,$logger) {
    $this->db = new DbHelper();
    $this->logger = $logger;
    $this->syncResults = array();
    $this->user = $user;
  }

public function synchronize($syncData) {
    $response = array();
    foreach ($syncData as $data) {
     array_push($response, $this->sync($data));
    }
  return $response;
}


  public function sync($syncData) {

    $table = $syncData->table;

    //first delete what is deleted
    $this->syncDelete($table, $syncData->trash);

    //theng get the new entries which are nt locally available
    $this->syncResults["new"] =  $this->getNew($table, $syncData->lastServerId);

    //Insert the new data from the local db
    $this->syncResults["sync"] = $this->syncNew($table, $syncData->newData);

    //Update entries which were updated locally
     $this->syncResults["updates"] = $this->syncUpdates($table, $syncData->updated);

    //Look for modifications or deletes that are not synced to local db and add them to the syncResults
    $this->syncDown($table, $syncData->localData);

    //Add the tablename to the results
    $this->syncResults['table'] = $table;

    return $this->syncResults;
  }


  //Get new items that are not in the local
  //Entries whole serverid is greater than the last serverid recieved on the in the local db
  private function getNew($table, $lastId) {

    if($lastId == null){
      $lastId = 0;
    }

    // $this->syncResults["new"] = array();
    $selection = "serverId>? AND user=?";
    $projection = "*";
    $selectionArgs = [0=>$lastId,1=>$this->user];
    $n =  $this->db->query($table, $projection, $selection, $selectionArgs, null,null,null,TRUE);

    if(!$n){
    $this->logger->error($this->db->message);
    }

    return $n;
  }

  //Insert new entries from thelocal database
  private function syncNew($table, $d) {
    $new = array();

    // $this->logger->info(json_encode($d));
    foreach ($d as $values) {
      $values = (array) $values;
      $n = array();

      //Get the local id
      $n["id"] = $values['id'];

      //Set the last modified time
      $lastModified = time() * 1000;
      $values["lastModified"] = $lastModified;

      //Add a user
      $values["user"] = $this->user;

       //Add a id
      $server_id = date("Ymdhis")+$this->user;
      $values["serverId"] = $server_id;

      $n['lastModified'] = $lastModified;

      // //Remove the temp server_id from the insert values
      // unset($values['serverId']);

      /*Check if the entry exists,
      * Due to parse error on the local device, the sync results may not be stored in the local database
      * which can lead to doulbe entries on the server.
      */
      $item =$this->db->query($table,"serverId","id=?",[0=>$values['id']]);

      $this->logger->info("........Item.........".json_encode($item));

     // if entry exists already
      if($item && $item != -1){

        $this->logger->info("Item ".$values["id"]." with serverId ".$item["serverId"]." Already exists");
        $n["serverId"] = $item["serverId"];

      }else{

        $this->logger->info("Item clear sync...");
        //Insert the new entry which retuns the real server id
        $results  = $this->db->insert($table, $values);

       if(!$results){
         $this->logger->error("Sync :: ".$this->db->message);
         $n["serverId"] = $server_id;

       }else{
         $n["serverId"] = $server_id;
         //$this->db->query($table,"serverId","id=?",[0=>$results])['serverId'];
       }

      }

      //Add the entry details to the results
      array_push($new, $n);
    }
    return $new;
  }

  //Make the updates from any of the devices available on the server
  private function syncUpdates($table, $d) {

    $updates = array();

    foreach ($d as $values) {

      $u = array();
      $values = (array) $values;
      $selection = "serverId=?";
      $selectionArgs[0] = $values['serverId'];

      $lastMod = time() * 1000;
      $values["lastModified"] = $lastMod;

      //Add the local id to the response
      $u['id'] = $values['id'];

      //Add the last modified time to the response
      $u['lastModified'] = $lastMod;

      //Remove the local id and the temp serverId
      unset($values['serverId']);
      unset($values['id']);

      if ($this->db->update($table, $selection, $selectionArgs, $values)) {
        array_push($updates, $u);
      }

    }

   return  $updates;
  }

  //Compare the local data with the data on the server for any modifications
  //Check for updatedor deleted entries
  private function syncDown($table, $d) {
    // $this->logger->info("LocalData $table ".json_encode($d));
    $modified = array();
    $trash = array();

    foreach ($d as $data) {

      $data = (array) $data;
      $selectionArgs = [0=>$data['serverId']];
      $selection = "serverId=?";
      $projection = "*";

      $row = $this->db->query($table, $projection, $selection, $selectionArgs);
      // $this->logger->info("DownResults: $table ".json_encode($row));

      if ($row>0) {
        //if the field has been modified,add it to the sync results as modified
        if ($row['lastModified'] > $data['lastModified']) {
          array_push($modified, $row);
        }

      }

      //If the entry was deleted, add it's id to the syncresults as trash
      else if($row == '-1') {
        // $this->logger->info("Trash $table ".$data['serverId']);
        array_push($trash, $data['id']);
      }

    }

    $this->syncResults["modified"] = $modified;
    $this->syncResults["trash"] = $trash;
  }


//Delete what was deleted from any of the devices
  private function syncDelete($table, $d) {
    //for each of the ids on the array, delete the cooresponding entry
    foreach ($d as $id) {
      $selection = "serverId=? AND user = ?";
      $selectionArgs = [0=>$id,1=>$this->user];
      $this->db->delete($table, $selection, $selectionArgs);
    }
  }


  private function deletePhoto($table, $id) {
    $selectionArgs[0] = $id;
    $projection = "photo,thumbnail";
    $selection = "id=?";
    $row = $this->db->query($table, $projection, $selection, $selectionArgs);
    $path = $row['photo'];
    $thumb = $row['thumbnail'];

    if (file_exists($path)) {
      unlink($path);
    }

    if (file_exists($thumb)) {
      unlink($thumb);
    }

    return $this->db->syncDelete($table, $id);
  }

}