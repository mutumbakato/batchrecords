<?php
/**
*User model
*/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__ .'/../utils/mailer/Exception.php';
require __DIR__ .'/../utils/mailer/PHPMailer.php';
require __DIR__ .'/../utils/mailer/SMTP.php';

class User {

  private $db;
  private $logger;

  public function __construct($logger) {
    $this->db = new DbHelper();
    $this->logger = $logger;
  }

  public function createAccount($data){

    $data = (Array)$data;
    $password = $data['password'];
    $username = $data['username'];

    $email = $data['email'];
    $phone = $data['phone'];

    //Hash the password before storing
    $data['password'] = Password::hash($password);
    $data['create_date'] = date('Y-m-d H:i:s');

    $id = $this->getUniquId();

    $data['id'] = $id;
    $results = $this->db->insert(TB_USERS,$data);

    if (!$results) {
      $this->logger->error("User-createAccount :: ".$this->db->message);
      //return $results;
    }

    //Log user in and return login details
    return $this->login($email,$password) ;
  }

  private function getUniquId(){
        $id = date("Ymdhis");
        return $id;
  }


  public function login($email,$password){

    $results =  $this->db->query(TB_USERS,"id,username,email,phone,password,create_date","email=? OR phone=?",[0=>$email,1=>$email]);

    if (!$results || $results == NOT_FOUND) {
      $this->logger->error("Login ::$results ::".$this->db->message);
      return $results;
    }

    //Compare the password with the hash
    $_password = $results['password'];

    if (!Password::check_password($_password,$password)) {
      return NOT_FOUND;
    }

    //remove massword from results
    unset($results['password']);

    //Generate and store a login_token
    $login_token = $this->generateLoginToken();
    $login_date = date('Y-m-d H:i:s');
    $user_id = $results['id'];
    $server_id = date("Ymdhis")+$results['id'];

    $licence= new Licence($this->logger);
    $licene_status = $licence->getLicence($user_id);
    $values = ['id'=>$server_id,'user_id'=>$user_id,'token'=>$login_token,'login_date'=>$login_date];

    $login = $this->db->insert(TB_LOGINS,$values);

    //Check if token was stored successfully
    if (!$login) {
      $this->logger->error($this->db->message);
      //return $login;
    }

    //Return user details plus login_token
    $details = ['id'=>$results['id'],'username'=>$results['username'],'email'=>$results['email'],'token'=>$login_token,'licence'=>$licene_status,'create_date'=>$results['create_date']];

    // $results["token"] = $login_token;
    // $subscriptions = new Subscription($this->logger);
    // $results["licence"] = $subscriptions->getLiscence($results["id"]);

    return $details;
  }

  public function logOut($token){
    $results = $this->db->query(TB_LOGINS,"id","token=?",[0=>$token]);
    if (!$results || $results == NOT_FOUND) {
      return $results;
    }
    return $this->db->delete(TB_LOGINS,'id=?',[0=>$results["id"]]);
  }

  /**
  *Get all registerd users
  *@param page:page number, if not provided returns the first 30 records
  *@return registered users
  */
  public function getAll($page=1){
    $page = $page-1;
    $limit = (LIMIT*$page).','.LIMIT;
    return $this->db->query(TB_USERS,"id,username,email,phone",null,null,null,null,$limit);
  }

  /**
  *Get user details
  *@param id:user id
  *@return user detaild with id @param
  */
  public function getDetails($id) {
    $results = $this->db->query(TB_USERS,'*',"id=?",[0=>$id]);
    if (!$results) {
      $this->logger->error("User-getDetails :: ".$this->db->message);
    }
    return $results;
  }


  public function findByUsername($username) {
    $results = $this->db->query(TB_USERS,'id,username,email,phone',"username=?",[0=>$username]);
    if (!$results) {
      $this->logger->error("User-findByUsername :: ".$this->db->message);
    }
    return $results;
  }

  public function findByEmail($email) {
    $results = $this->db->query(TB_USERS,'id,username,email,phone',"email=?",[0=>$email]);
    if (!$results) {
      $this->logger->error("User-findByEmail:: ".$this->db->message);
    }
    return $results;
  }

  public function findByPhone($phone) {
    $results = $this->db->query(TB_USERS,'id,username,email,phone',"phone=?",[0=>$phone]);
    if (!$results || $results == NOT_FOUND) {
      $this->logger->error("User-findByPhone:: ".$this->db->message);
    }
    return $results;
  }

  public function getUserName($email) {
    $results = $this->db->query(TB_USERS,'username',"email=?",[0=>$user_id]);
    if (!$results) {
      $this->logger->error("User-getUserName :: ".$this->db->message);
    }
    return $results['username'];
  }

  public function verify($phone,$code){
    $results = $this->findByPhone($phone);
    if (!$results || $results == NOT_FOUND) {
      $this->logger->info("Verify :$phone Results = ".$results);
     }
     return $results;
  }

  public function isEmailExist($email){
    $results = $this->findByEmail($email);
    return ($results && $results!=NOT_FOUND);
  }

  public function isPhoneExist($phone){

    $results = $this->findByPhone($phone);
    return ($results && $results!=NOT_FOUND);
  }

  public function isUserNameExist($username){

    $results = $this->findByUsername($username);
    return ($results && $results!=NOT_FOUND);
  }


  //Authenticate user with login token
  public static function auth($logger,$token){

    $db = new DbHelper();
    $projection = "user_id";
    $Selection = "token=?";
    $args =[0=>$token];
    $results = $db->query(TB_LOGINS,$projection,$Selection,$args);

    if(!$results || $results == NOT_FOUND){
      $logger->error($db->message);
      return $results;
    }

    return $results['user_id'];
  }


  //Generate login token
  public function generateLoginToken(){
    $token = md5(uniqid(rand(), true));
    return $token;
  }


  public function updatePassword($new_password,$token){

         $results =  $this->db->query(TB_USERS,"password","password_reset_token=?",[0=>$token]);

        if (!$results || $results == NOT_FOUND) {
              $this->logger->error("Reset ::$results ::".$this->db->message);
              return $results;
        }

            $new_password = Password::hash($new_password);
            $results= $this->db->update(TB_USERS,"password_reset_token=?",[0=>$token],['password'=>$new_password,'password_reset_token'=>""]);

            if($results){
             return  "Your password has been reset,you can go back and log in.";
            }

            return $results;
      }


  //Send password reset request email
  public function resetPassword($email){

    if(!$this->isEmailExist($email)){
      return NOT_FOUND;
    }

    $token = $this->generateLoginToken();
    $results = $this->db->update(TB_USERS,"email=?",[0=>$email],["password_reset_token"=>$token]);

    if(!$results){
        $this->logger->error($this->db->message);
        return 0;
    }

    return $this->sendGrid($email,$token);
  }

private function sendEmail($email,$token){

        //Create a new PHPMailer instance
      $mail = new PHPMailer;
      //Tell PHPMailer to use SMTP
      $mail->isSMTP();
      //Enable SMTP debugging
      // $mail->SMTPDebug = 2;
      //Set the hostname of the mail server
      $mail->Host = 'smtp.gmail.com';
      // use
      // $mail->Host = gethostbyname('smtp.gmail.com');
      // if your network does not support SMTP over IPv6
      //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
      $mail->Port = 587;
      //Set the encryption system to use - ssl (deprecated) or tls
      $mail->SMTPSecure = 'tls';
      //Whether to use SMTP authentication
      $mail->SMTPAuth = true;
      //Username to use for SMTP authentication - use full email address for gmail
      $mail->Username = "mutumbakato@gmail.com";
      //Password to use for SMTP authentication
      $mail->Password = "com.jcil.gugo";
      //Set who the message is to be sent from
      $mail->setFrom('poultrybatchmanager@gmail.com', 'Poultry Batch manager');
      //Set an alternative reply-to address
      $mail->addReplyTo('mutumbakato@gmail.com');
      //Set who the message is to be sent to
      $mail->addAddress($email);
      //Set the subject line
      $mail->Subject = 'Password reset';

      //convert HTML into a basic plain-text alternative body
      $mail->isHTML(true); // Set email format to HTML

      $mail->Body ="<p><strong>Hello!</strong></p> <p>We heard you forgot your password".
      ", please use the link below to create a new one.</p>".
      "<br><p>Password reset link: https://batchrecords.herokuapp.com/user/password/reset/$token".
      "<br><p>If you did not send this request please ignore.</p>";

      $mail->AltBody = "Hello!\nWe heard you forgot your password".
      ", please use the link below to create a new one.</p>".
      "\nPassword reset link: https://batchrecords.herokuapp.com/user/password/reset/$token".
      "\n\nIf you did not send this request please ignore.";
      //Attach an image file

      //send the message, check for errors

      if (!$mail->send()) {
          return "Failed to send email, Please try again.";
      } else {
          return "A link to reset your password has been sent to $email";
      }

}

private function sendGrid($email,$token){

    $message = "Hello.\n\nWe heard you forgot your password".
    ", please use the link below to create a new one.".
    "\n\nPassword reset link: https://batchrecords.herokuapp.com/user/password/reset/$token".
    "\n\nIf you did not send this request please ignore.";

    $from = new SendGrid\Email("Poultry Batch Manager", "support@poultrybatchmanager.com");
    $subject = "Password reset.";
    $to = new SendGrid\Email("User", "$email");
    $content = new SendGrid\Content("text/plain", "$message");
    $mail = new SendGrid\Mail($from, $subject, $to, $content);

    $apiKey = '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';
    $sg = new \SendGrid($apiKey);
    $response = $sg->client->mail()->send()->post($mail);

    if($response->statusCode()==202){
      return "We have sent a password reset link to $email";
    }else{
      return "Password reset has failed, please try again.";
    }

   }

}

?>
