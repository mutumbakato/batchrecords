<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register model
require __DIR__ . '/../src/model.php';


//Register dbhelper
require __DIR__.'/../src/db/dbhelper.php';



// Register routes
// require __DIR__ . '/src/routes.php';

// Register password
// require __DIR__ . '/../src/utils/password.php';

//Register all routes
foreach (glob( __DIR__ . '/../src/routes/*.php') as $filename){
    require $filename;
}



//Register all models
foreach (glob( __DIR__ . '/../src/models/*.php') as $filename){
    require $filename;
}

// Run app
$app->run();
