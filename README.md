# Poultry Batch manager API
Poultry Batch manager is a book keeping app for commercial poultry farmers.

## Setup the Application

To insatll dependencies

    phph composer install

To run the application in development, you can also run this command. 

	php composer start

Run this command to run the test suite

	php composer test
